import React, { Component } from 'react';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import Link from '@material-ui/core/Link';
import List from '@material-ui/core/List';
import { withStyles } from '@material-ui/core/styles';
import ViewCompact from '@material-ui/icons/ViewCompact';
import InsertChartOutlined from '@material-ui/icons/InsertChartOutlined';
import GridOn from '@material-ui/icons/GridOn';
import LinearScale from '@material-ui/icons/LinearScale';

import Edit from '@material-ui/icons/Edit';

import './side-menu.scss';

const styles = theme => ({
  menuColor: {
    color: '#ABABAD'
  }
});

class SideMenu extends Component {
  constructor(props) {
    super(props);

    this.handleOptionEditor = this.handleOptionEditor.bind(this);
    this.handleOptionCards = this.handleOptionCards.bind(this);
    this.handleOptionChain = this.handleOptionChain.bind(this);
    this.handleOptionTable = this.handleOptionTable.bind(this);
    this.handleOptionGraph = this.handleOptionGraph.bind(this);

  }
  handleOptionEditor(e) {
    return this.props.handleComponentOption('editor');
  }
  handleOptionCards(e) {
    return this.props.handleComponentOption('cards');
  }
  handleOptionChain(e) {
    return this.props.handleComponentOption('chain');
  }
  handleOptionTable(e) {
    return this.props.handleComponentOption('table');
  }
  handleOptionGraph(e) {
    return this.props.handleComponentOption('graph');
  }

  render() {
    const { classes, theme } = this.props;

    return(
      <List>
        <Link value="cards" onClick={this.handleOptionCards}>
          <ListItem button key='Templates'>
            <ListItemIcon className={classes.menuColor}>
              <ViewCompact />
            </ListItemIcon>
              <ListItemText classes={{primary:classes.menuColor}} primary='Templates' />
          </ListItem>
        </Link>
        <Link value="editor" onClick={this.handleOptionEditor}>
          <ListItem button key='Editor'>
            <ListItemIcon className={classes.menuColor} name="template-create">
              <Edit name="template-create"/>
            </ListItemIcon>
              <ListItemText classes={{primary:classes.menuColor}} primary='Editor' />
          </ListItem>
        </Link>
        <Link value="chain" onClick={this.handleOptionChain}>
          <ListItem button key='Red Blockchain'>
            <ListItemIcon className={classes.menuColor}>
              <LinearScale />
            </ListItemIcon>
              <ListItemText classes={{primary:classes.menuColor}} primary='Red Blockchain' />
          </ListItem>
        </Link>
        <Link value="table" onClick={this.handleOptionTable}>
          <ListItem button key='Recipientes'>
            <ListItemIcon className={classes.menuColor}>
              <GridOn />
            </ListItemIcon>
              <ListItemText classes={{primary:classes.menuColor}} primary='Recipientes' />
          </ListItem>
        </Link>
        <Link value="graph" onClick={this.handleOptionGraph}>
          <ListItem button key='Analytics'>
            <ListItemIcon className={classes.menuColor}>
              <InsertChartOutlined />
            </ListItemIcon>
              <ListItemText classes={{primary:classes.menuColor}} primary='Analytics' />
          </ListItem>
        </Link>
      </List>
    );
  }
}

export default withStyles(styles, { withTheme: true })(SideMenu);