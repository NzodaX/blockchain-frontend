import React, { Component } from 'react';
import './Cards-Chooser.scss';
import cert from '../../assets/img/cert.png';
import mas from '../../assets/img/mas.svg';

export class Cards extends Component {
  cardOption = () => {
    this.props.handleCardOption(true);
    this.props.handleComponentOption('editor');
  };

  cardOptionFalse = () => {
    this.props.handleCardOption(false);
    this.props.handleComponentOption('editor');
  };

  render() {
    return(
      <div className="Cards">
        <h1 className="templates-title">Templates</h1>
        <section id="events" className="event-list ">
          <div className="cards-container">
            <button onClick={this.cardOption}>
              <article className="event" >
                <figure className="event-imageContainer" >
                  <img className="event-image" src={ cert } alt="Platziconf" width="500"/>
                </figure>
                <div className="event-details">
                  <h3 className="event-title">Gobierno</h3>
                  <p className="event-description">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s</p>
                </div>
              </article>
            </button>
            <button onClick={this.cardOption}>
              <article className="event" >
                <figure className="event-imageContainer" >
                  <img className="event-image" src={ cert } alt="Platziconf" width="500"/>
                </figure>
                <div className="event-details">
                  <h3 className="event-title">Educación</h3>
                  <p className="event-description">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s</p>
                </div>
              </article>
            </button>
            <button onClick={this.cardOption}>
              <article className="event" >
                <figure className="event-imageContainer" >
                  <img className="event-image" src={ cert } alt="Platziconf" width="500"/>
                </figure>
                <div className="event-details">
                  <h3 className="event-title">Agricultura</h3>
                  <p className="event-description">Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s</p>
                </div>
              </article>
              </button>
              <button className="mas-event" onClick={this.cardOptionFalse}>
                <figure className="mas-imageContainer">
                  <img className="mas" src={ mas } alt="boton_mas"/>
                </figure>
                  <h3 className="mas-title">Crea un nuevo Certificado</h3>
              </button>
          </div>
        </section>
      </div>
    );
  }
}