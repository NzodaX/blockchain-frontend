import React, { Component } from "react";
import NavigateBeforeIcon from '@material-ui/icons/NavigateBefore';
import Chart from "react-apexcharts";
import './chart.scss'
let colors = ["#dc0061"];
    
class Charts extends Component {
  constructor(props) {
    super(props);

    this.state = {
      options: {
        chart: {
          id: "basic-bar"
        },
        xaxis: {
          categories: [1991, 1992, 1993, 1994, 1995, 1996, 1997, 1998, 1999]
        },
        colors: colors,
      },
      series: [
        {
          name: "series-1",
          data: [30, 40, 45, 50, 49, 60, 70, 91]
        }
      ],
    };
  }
  prevStep = () => {
    // this.props.history.push('/issue-records');
    return this.props.handleComponentOption('table'); 
  };

  render() {
    return (
      <div className="app">
        <div className="row">
          <div className="textos-table">
            <h1 className="margin-none-table">Analytics</h1>
            <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry. </p>
          </div>
          <div className="large">
            <div className="prev-button" onClick={ this.prevStep.bind(this) }>
              <NavigateBeforeIcon />
              Atrás
            </div>
          </div>
          <div className="mixed-chart flex">
            <Chart
              options={this.state.options}
              series={this.state.series}
              type="bar"
              width="700"
              
            />
          </div>
          <div className="flex">
            <Chart
              options={this.state.options}
              series={this.state.series}
              type="line"
              width="400"
            />
            <Chart
              options={this.state.options}
              series={this.state.series}
              type="radar"
              width="400"
            />
          </div>
        </div>
      </div>
    );
  }
}

export default Charts;