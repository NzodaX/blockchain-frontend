import React from 'react'
import { BrowserRouter, Route, Switch, Link } from 'react-router-dom'

import Login from  '../../pages/login/login';
import RedBlockchain from  '../../pages/red-blockchain/blockchain';
import Template from '../../pages/template/template';
import Preview from '../../pages/preview/preview';
import ManageRecipients from '../../pages/manage-recipients/recipients';
import TemplateChooser from '../../pages/template-chooser/template-chooser';
import Charts from '../../components/Chart/charts';

const Routes = () => (
  <BrowserRouter>
    <Switch>
      <Route exact path='/' component={Login} />
      <Route path='/issue-records' component={RedBlockchain} />
      <Route path='/create-template' component={Template} />
      <Route path='/view-template' component={Preview} />
      <Route path='/recipients' component={ManageRecipients} />
      <Route path='/chooser' component={TemplateChooser} />
      <Route path='/chart' component={Charts} />
    </Switch>
  </BrowserRouter>
)

export default Routes