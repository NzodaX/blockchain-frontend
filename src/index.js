import React from 'react';
import ReactDOM from 'react-dom';
import Routes from './components/routes/routes';
import '@babel/polyfill'

ReactDOM.render(
  <Routes />,
  document.getElementById("root")
);
