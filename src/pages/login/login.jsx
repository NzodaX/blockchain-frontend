import React, { Component } from 'react';

import logo from '../../assets/img/logoOsCity.png'
import './login.scss';

let container;

class Login extends Component {
    constructor(props) {
        super(props);
        this.state = {
            fields: {
                email: '',
                password: '',
                name: ''
            },
            errors: {}
        };
        this.isSignUp = false;
    }

    handleChange(e) {
        let fields = this.state.fields;
        fields[e.target.name] = e.target.value;
        this.setState({
            fields: fields,
            errors: {}
        });
  
    }
  
    submitForm(e, isSignUp) {
        e.preventDefault();
        if (this.validateForm(isSignUp)) {
            let fields = {};
            fields['email'] = '';
            fields['password'] = '';
            fields['name'] = '';
            this.setState({
                fields: fields,
                errors: {}
            });
            this.props.history.push('/chooser');
        }
    }

    validateForm(isSignUp) {
        let fields = this.state.fields;
        let errors = {};
        let formIsValid = true;
  
        if (!fields['email']) {
          formIsValid = false;
          errors['email'] = '*Por favor ingresa un email.';
        }
        if (!fields['name'] && isSignUp) {
            formIsValid = false;
            errors['name'] = '*Por favor ingresa un nombre.';
          }
        if (!fields['password']) {
          formIsValid = false;
          errors['password'] = '*Por favor ingresa una contraseña.';
        }
        this.setState({
          errors: errors
        });
        return formIsValid;
    }

    signUpMove() {
        container.classList.add('right-panel-active');
    }
    signInMove() {
        container.classList.remove('right-panel-active');
    }
    getContainer() {
        container = document.getElementById('container');
    }

  render() {
    
    return (
        <div className="login">
            <div className="container" id="container" onFocus={ this.getContainer }>
                <div className="form-container sign-up-container">
                    <form onSubmit={ (e) => { this.submitForm(e, true) } }>
                        <h1>Crea tu Cuenta</h1>
                        <input
                            type="text"
                            placeholder="Name"
                            value={ this.state.fields.name }
                            onChange={ this.handleChange.bind(this) }
                            name="name"
                        />
                        <div className="error-form">{ this.state.errors.name }</div>
                        <input
                            type="email"
                            placeholder="Email"
                            value={ this.state.fields.email }
                            onChange={ this.handleChange.bind(this) }
                            name="email"
                        />
                        <div className="error-form">{ this.state.errors.email }</div>
                        <input
                            type="password"
                            placeholder="Password"
                            value={ this.state.fields.password }
                            onChange={ this.handleChange.bind(this) }
                            name="password"
                        />
                        <div className="error-form">{this.state.errors.password}</div>
                        <button className="enter-loginButton" >Crear</button>
                    </form>
                </div>
                <div className="form-container sign-in-container">
                    <form onSubmit={ (e) => { this.submitForm(e, false) } }>
                        <h1>Inicia Sesión</h1>
                        <input
                            type="email"
                            placeholder="Email"
                            value={ this.state.fields.email }
                            onChange={ this.handleChange.bind(this) }
                            name="email"
                        />
                        <div className="error-form">{ this.state.errors.email }</div>
                        <input
                            type="password"
                            placeholder="Password"
                            value={ this.state.fields.password }
                            onChange={ this.handleChange.bind(this) }
                            name="password"
                        />
                        <div className="error-form">{ this.state.errors.password }</div>
                        <a href="#">¿Olvidaste tu contraseña?</a>
                        <button className="enter-loginButton" type="submit">Entrar</button>
                    </form>
                </div>
                <div className="overlay-container">
                    <div className="overlay">
                        <div className="overlay-panel overlay-left">
                            <figure>
                                <img src={ logo } alt="" width="150px" height="100px"/>
                            </figure>
                            <h1>¡ Hola de Nuevo !</h1>
                            <p>Para mantenerte conectado con nosotros por favor accede con tu información personal</p>
                            <button className="ghost" id="signIn" onClick={ this.signInMove.bind(this) }>Entrar</button>
                        </div>
                        <div className="overlay-panel overlay-right">
                            <figure>
                                <img src={ logo } alt="" width="150px" height="100px"/>
                            </figure>
                            <h1>¡ Hey, Amigo !</h1>
                            <p>Ingresa tus detalles personales e inicia un viaje con nosotros</p>
                            <button className="ghost" id="signUp" onClick={ this.signUpMove.bind(this) }>Registro</button>
                        </div>
                    </div>
                </div>
            </div>
      </div>
    );
  }
}

export default Login;
