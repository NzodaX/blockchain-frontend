import './template-chooser.scss';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import { withStyles } from '@material-ui/core/styles';
import Drawer from '@material-ui/core/Drawer';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import CssBaseline from '@material-ui/core/CssBaseline';
import Typography from '@material-ui/core/Typography';
import Divider from '@material-ui/core/Divider';
import IconButton from '@material-ui/core/IconButton';
import MenuIcon from '@material-ui/icons/Menu';
import ChevronLeftIcon from '@material-ui/icons/ChevronLeft';
import ChevronRightIcon from '@material-ui/icons/ChevronRight';
import React, { Component } from 'react';
import logoOsCity from '../../assets/img/Logo_os_color.svg';
import { Cards } from '../../components/Cards-Choooser/Cards';
import SideMenu from '../../components/side-menu/side-menu';
import RedBlockchain from '../../pages/red-blockchain/blockchain';
import ManageRecipients from '../../pages/manage-recipients/recipients';
import Template from '../../pages/template/template';
import Charts from '../../components/Chart/charts'
import Preview from '../../pages/preview/preview'

let drawerWidth = 265;
      
const styles = theme => ({
  root: {
    display: 'flex',
  },
  appBar: {
    zIndex: theme.zIndex.drawer + 1,
    transition: theme.transitions.create(['width', 'margin'], {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.leavingScreen,
    }),
    backgroundColor: '#EFEFEF',
  },
  appBarShift: {
    marginLeft: drawerWidth,
    width: `calc(100% - ${drawerWidth}px)`,
    transition: theme.transitions.create(['width', 'margin'], {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.enteringScreen,
    }),
  },
  menuButton: {
    marginLeft: 12,
    marginRight: 36,
    color: '#F06499'
  },
  hide: {
    display: 'none',
  },
  drawer: {
    width: drawerWidth,
    flexShrink: 0,
    whiteSpace: 'nowrap',
  },
  drawerOpen: {
    width: drawerWidth,
    transition: theme.transitions.create('width', {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.enteringScreen,
    }),
    backgroundColor: '#303135',
    width: '265px'
  },
  drawerClose: {
    transition: theme.transitions.create('width', {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.leavingScreen,
    }),
    overflowX: 'hidden',
    width: theme.spacing.unit * 7 + 1,
    [theme.breakpoints.up('sm')]: {
      width: theme.spacing.unit * 9 + 1,
    },
    backgroundColor: '#303135',
    width: '56px !important',
  },
  toolbar: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'flex-end',
    padding: '0 8px',
    backgroundColor: '#EFEFEF',
    height: '64px'
  },
  toolbarIcon: {
    color: '#F06499'
  },
  typography: {
    width: '100%',
    textAlign: 'center',
    margin: '0 auto',
    verticalAlign: 'middle',
    color: '#F06499',
  },
  content: {
    flexGrow: 1,
    padding: theme.spacing.unit * 3,
    paddingTop: '4.5%',
  }
});
  
class TemplateChooser extends Component {
    state = {
      open: false,
      componentOption: 'cards',
      cardOption: false
    };
  
    handleDrawerOpen = () => {
      this.setState({ open: true });
    };
  
    handleDrawerClose = () => {
      this.setState({ open: false });
    };

    handleComponentOption = (option) => {
      this.setState({ componentOption: option });
    }

    handleLogo = () => {
      this.setState({ componentOption: 'cards' });
    }

    handleCardOption = (option) => {
      this.setState({
        cardOption: option
      });
    }
  
    render() {
      const { classes, theme } = this.props;
  
      return (
        <div className={classes.root}>
          <CssBaseline />
          <AppBar
            position="fixed"
            className={classNames(classes.appBar, {
              [classes.appBarShift]: this.state.open,
            })}
          >
            <Toolbar disableGutters={!this.state.open}>
              <IconButton
                color="inherit"
                aria-label="Open drawer"
                onClick={this.handleDrawerOpen}
                className={classNames(classes.menuButton, {
                  [classes.hide]: this.state.open,
                })}
              >
                <MenuIcon/>
              </IconButton>
              <Typography variant="h6" color="inherit" noWrap className={classNames(classes.typography)}>
                <img onClick={ this.handleLogo.bind(this) } className="os-logo" src={ logoOsCity } alt="oscity_logo" width="135px"></img>
              </Typography>
            </Toolbar>
          </AppBar>
          <Drawer
            variant="permanent"
            className={classNames(classes.drawer, {
              [classes.drawerOpen]: this.state.open,
              [classes.drawerClose]: !this.state.open,
            })}
            classes={{
              paper: classNames({
                [classes.drawerOpen]: this.state.open,
                [classes.drawerClose]: !this.state.open,
              }),
            }}
            open={this.state.open}
          >
            <div className={classes.toolbar}>
              <IconButton onClick={this.handleDrawerClose} className={classes.toolbarIcon}>
                {theme.direction === 'rtl' ? <ChevronRightIcon /> : <ChevronLeftIcon />}
              </IconButton>
            </div>
            <Divider />
            <SideMenu handleComponentOption={this.handleComponentOption}/>
            <Divider />
          </Drawer>
          <main className={classes.content}>
            {(() => {
              switch (this.state.componentOption) {
                case "cards"  : return <Cards handleCardOption={this.handleCardOption} handleComponentOption={this.handleComponentOption}/>;
                case "editor" : return <Template handleComponentOption={this.handleComponentOption} stateCard={this.state.cardOption} />;
                case "chain"  : return <RedBlockchain handleComponentOption={this.handleComponentOption}/>;
                case "table"  : return <ManageRecipients handleComponentOption={this.handleComponentOption}/>;
                case "graph"  : return <Charts handleComponentOption={this.handleComponentOption}/>;
                case "preview": return <Preview handleComponentOption={this.handleComponentOption}/>
                default       : return  <Cards/>;
              }
            })()}
          </main>
        </div>
      );
    }
  }
  
  TemplateChooser.propTypes = {
    classes: PropTypes.object.isRequired,
    theme: PropTypes.object.isRequired,
  };
  
  export default withStyles(styles, { withTheme: true })(TemplateChooser);